package com.example.bigdata.repository;

import com.example.bigdata.model.mongodb.VkApp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VkAppRepository extends MongoRepository<VkApp, String> {
    VkApp findVkAppById(String id);
}
