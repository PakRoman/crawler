package com.example.bigdata.repository;

import com.example.bigdata.model.mongodb.ProfileTask;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileTaskRepository extends MongoRepository<ProfileTask, String> {
}
