package com.example.bigdata.repository;

import com.example.bigdata.model.mongodb.UserSubscription;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupRepository extends MongoRepository<UserSubscription, String>{

}
