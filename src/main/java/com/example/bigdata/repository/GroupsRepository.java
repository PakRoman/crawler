package com.example.bigdata.repository;

import com.example.bigdata.model.mongodb.Subscriptions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupsRepository extends MongoRepository<Subscriptions, Integer>{
    Subscriptions findById(int id);
}
