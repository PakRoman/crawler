package com.example.bigdata.repository;

import com.example.bigdata.model.mongodb.StatusTask;
import com.example.bigdata.model.mongodb.SubscriptionTask;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionTaskRepository extends MongoRepository<SubscriptionTask, String> {
    List<SubscriptionTask> findByStatusTask(StatusTask statusTask);
}
