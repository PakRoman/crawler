package com.example.bigdata.services;

import com.example.bigdata.services.vk.Friends;
import com.example.bigdata.services.vk.Posts;
import com.example.bigdata.services.vk.Profile;
import com.example.bigdata.services.vk.Subscription;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserParser {
    @Autowired
    Profile profile;
    @Autowired
    Subscription subscription;
    @Autowired
    Friends friends;
    @Autowired
    Posts posts;

    public void profileParserWorker(Integer id,
                                    String access_token,
                                    int app_id) throws ClientException, ApiException, IOException {

        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);
        ServiceActor actor = new ServiceActor(app_id, access_token);
        //Get profile
        profile.getUserProfile(String.valueOf(id), vk, actor);
        //Get friends
        friends.getUserFriends(id, vk, actor);
        //Get subscription
        subscription.getUserSubscriptions(id, vk, actor);
        //Получение списка постов со стены пользователя
        posts.getPosts(id, vk, actor);
    }

    public void subscriptionParserWorker(Integer id,
                                         String access_token,
                                         int app_id) throws IOException, ClientException, ApiException {

        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);
        ServiceActor actor = new ServiceActor(app_id, access_token);
        try {
            subscription.getSubscription(String.valueOf(id), vk, actor);
            posts.getPosts(id * -1, vk, actor);
        } catch (NullPointerException e) {
            System.out.println("Error");
        }
    }
}
