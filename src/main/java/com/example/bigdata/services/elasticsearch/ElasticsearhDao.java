package com.example.bigdata.services.elasticsearch;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Service DAO for work with elasticsearch
 */
@Service
public class ElasticsearhDao {

    /**
     * Init RestClient for elasticsearch
     */
    @Autowired
    private RestHighLevelClient restClient;

    /**
     * Logger in spring boot
     */
    Logger logger = LoggerFactory.getLogger(ElasticsearhDao.class);

    /**
     * Method for save users profile in elasticsearch
     * @param id primary key for documents in indexcies "profile"
     * @param data profile model in Json
     * @throws IOException
     */
    public void saveProfile(String id, String data) throws IOException{
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("profile")
                .id(id)
                .source(data, XContentType.JSON);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException  e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }

    public void saveFriends(String id, String data) throws IOException {
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("friends")
                .id(id)
                .source("friends", data);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }

    public void saveSubscription(String id, String data) throws IOException {
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("subscription")
                .id(id)
                .source(data, XContentType.JSON);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");

            }
        }
    }

    public void savePosts (String id, String post, String data, String user) throws IOException {
        //Timestamp version = new Timestamp(System.currentTimeMillis());
        Map<String, Object> map = new HashMap<>();
        map.put("post", data);
        map.put("user", user);
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("posts")
                .id(id + "/" + post)
                //.version(version.getTime())
                //.versionType(VersionType.EXTERNAL)
                .source(map);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }

    public void savePostLikes (String idGroup, String idPost, String data) throws IOException {
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("likes")
                .id(idGroup + "/" + idPost)
                .source("users", data);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }

    public void savePostsComments (String idGroup, String idPost, String data) throws IOException {
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("comments")
                .id(idGroup + "/" + idPost)
                .source(data, XContentType.JSON);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }

    public void savePostsReposts (String idGroup, String idPost, String data) throws IOException {
        //Create request to record
        IndexRequest indexRequest = new IndexRequest("reposts")
                .id(idGroup + "/" + idPost)
                .source(data, XContentType.JSON);
        try {
            //Request submission
            IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                logger.error("Ошибка");
            }
        }
    }
}
