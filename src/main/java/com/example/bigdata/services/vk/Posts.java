package com.example.bigdata.services.vk;

import com.example.bigdata.model.elasticsearch.UserPostModel;
import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.wall.WallPostFull;
import com.vk.api.sdk.objects.wall.responses.GetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
@Service
public class Posts {
    @Autowired
    private ElasticsearhDao elasticsearhDao;
    @Autowired
    private Likes likes;
    @Autowired
    private Comments comments;

    public void getPosts(int id,
                         VkApiClient vk,
                         ServiceActor actor )throws ClientException, IOException {
        UserPostModel userPostModel = new UserPostModel("");
        try {
            GetResponse getResponse = vk.wall().get(actor)
                    .ownerId(id)
                    .count(5)
                    .execute();
            List<WallPostFull> responseList = getResponse.getItems();
            Gson g = new Gson();
            for (int i = 0; i < responseList.size(); i++) {
                comments.getComments(responseList.get(i).getId(),
                                id,
                                vk,
                                actor);
                likes.getLikes(responseList.get(i).getId(),
                                id,
                                vk,
                                actor);
                responseList.get(i).getId();
                String user = g.toJson(userPostModel);
                String data = g.toJson(responseList.get(i));
                elasticsearhDao.savePosts(String.valueOf(id), String.valueOf(responseList.get(i).getId()) , data, user);
            }
        } catch (ApiException e) {
            if (e.getCode() == 18) {

            }
            if (e.getCode() == 19) {

            }
            if (e.getCode() == 30) {

            }
        }

    }

    public List getPostById(String id, VkApiClient vk, ServiceActor actor) throws ClientException, ApiException, IOException{
        /*TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);
        ServiceActor actor = new ServiceActor(app_id, access_token);*/
            List<WallPostFull> list =  vk.wall().getById(actor, id)
                    .copyHistoryDepth(2)
                    .execute();
        return list;
    }
}
