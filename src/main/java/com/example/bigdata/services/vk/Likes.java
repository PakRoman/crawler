package com.example.bigdata.services.vk;

import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.likes.responses.GetListResponse;
import com.vk.api.sdk.queries.likes.LikesType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service

public class Likes {
    @Autowired
    private ElasticsearhDao elasticsearhDao;

    public void getLikes(int idPost, int idGroup, VkApiClient vk, ServiceActor actor) throws ClientException, ApiException, IOException {
        GetListResponse response = vk.likes().getList(actor, LikesType.POST)
                        .ownerId(idGroup)
                        .itemId(idPost)
                        .execute();
        List <Integer> likes = response.getItems();
        Gson g = new Gson();
        String data = g.toJson(likes);
        System.out.println(data);
        elasticsearhDao.savePostLikes(String.valueOf(idGroup), String.valueOf(idPost), data);
    }
}
