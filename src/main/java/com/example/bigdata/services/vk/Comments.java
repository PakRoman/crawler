package com.example.bigdata.services.vk;

import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.wall.WallComment;
import com.vk.api.sdk.objects.wall.responses.GetCommentsExtendedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class Comments {
    @Autowired
    private ElasticsearhDao elasticsearhDao;

    public void getComments(int idPost, int idGroup, VkApiClient vk, ServiceActor actor) throws ClientException {

        try {
            GetCommentsExtendedResponse getCommentsExtendedResponse = vk.wall().getCommentsExtended(actor, idPost)
                    .ownerId(idGroup)
                    .count(100)
                    .needLikes(true)
                    .execute();
            List<WallComment> list = getCommentsExtendedResponse.getItems();
            Gson g = new Gson();
            for (int i = 0; i < list.size(); i++) {
                String data = g.toJson(list.get(i));
                elasticsearhDao.savePostsComments(String.valueOf(idGroup), String.valueOf(idPost), data);
            }
        } catch (IndexOutOfBoundsException | IOException e) {
            System.out.println("Error");
        } catch (ApiException a) {
            if (a.getCode() == 212) {
                System.out.println("Error");
            }
        }

    }
}
