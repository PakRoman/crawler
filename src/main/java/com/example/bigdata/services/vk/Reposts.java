package com.example.bigdata.services.vk;

import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.wall.WallPostFull;
import com.vk.api.sdk.objects.wall.responses.GetRepostsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class Reposts {
    @Value("${vk.client.token}")
    private String access_token;
    @Value("${vk.client.app_id}")
    private Integer app_id;
    @Autowired
    private ElasticsearhDao elasticsearhDao;
    public void getRepost(int idPost, int idGroup) throws ClientException {
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);
        ServiceActor actor = new ServiceActor(app_id, access_token);
        try {
            GetRepostsResponse getRepostsResponse = vk.wall().getReposts(actor)
                    .ownerId(idGroup)
                    .postId(idPost)
                    .count(1000)
                    .execute();
            List<WallPostFull> list = getRepostsResponse.getItems();
            Gson g = new Gson();
            if (list.size() != 0) {
                for (int i = 0; i < list.size(); i++) {
                    String data = g.toJson(list.get(i));
                    elasticsearhDao.savePostsReposts(String.valueOf(idGroup), String.valueOf(idPost), data);
                }
            }
        } catch (IndexOutOfBoundsException | IOException e) {
            System.out.println("Error");
        } catch (ApiException a) {
            if (a.getCode() == 30) {
                System.out.println("Error");
            }
        }
    }
}
