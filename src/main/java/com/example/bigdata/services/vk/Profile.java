package com.example.bigdata.services.vk;

import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;
import com.vk.api.sdk.queries.users.UsersNameCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class Profile {
   /* @Value("${vk.client.token}")
    private String access_token;
    @Value("${vk.client.app_id}")
    private Integer app_id;*/
    @Autowired
    private ElasticsearhDao elasticsearhDao;
    public void getUserProfile(String id, VkApiClient vk, ServiceActor actor) throws ClientException, ApiException, IOException {

        //Фильтрация полей для отображения
        List<UserField> userFieldList = new ArrayList<>();
        for (UserField fields : UserField.values()) {
            if (fields != UserField.COMMON_COUNT) {
                userFieldList.add(fields);
            }
        }

        //Получение пользователя из запроса
        List<UserXtrCounters> responseList  = vk.users().get(actor)
                .userIds(id)
                .fields(userFieldList)
                .nameCase(UsersNameCase.NOMINATIVE)
                .execute();

        //Преобразование полученного объекта в json типа string
        Gson g = new Gson();
        String string = g.toJson(responseList.get(0));
        elasticsearhDao.saveProfile(id, string);
    }
}
