package com.example.bigdata.services.vk;

import com.example.bigdata.model.mongodb.Subscriptions;
import com.example.bigdata.model.mongodb.UserSubscription;
import com.example.bigdata.repository.GroupsRepository;
import com.example.bigdata.repository.UserGroupRepository;
import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.groups.GroupFull;
import com.vk.api.sdk.objects.groups.GroupsArray;
import com.vk.api.sdk.objects.users.responses.GetSubscriptionsResponse;
import com.vk.api.sdk.queries.groups.GroupField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class Subscription {

    @Autowired
    private ElasticsearhDao elasticsearhDao;
    @Autowired
    private UserGroupRepository userGroupRepository;
    @Autowired
    private GroupsRepository groupsRepository;
    public void getUserSubscriptions(int id, VkApiClient vk, ServiceActor actor) throws ClientException {
        try {

            //Get id groups
            GetSubscriptionsResponse subscription = vk.users().getSubscriptions(actor)
                    .userId(id)
                    .execute();
            GroupsArray responseList = subscription.getGroups();
            List<Integer> listGroup = responseList.getItems();

            //Save groups of users
            saveUserSubscription(id, listGroup);

            //Save groups
            saveSubscription(listGroup);

        } catch (ApiException e) {
            if (e.getCode() == 30) {
                System.out.println("Error");
            }
        }
    }
    public void saveUserSubscription(int id, List<Integer> listGroups) {
        UserSubscription userSubscription = new UserSubscription(id, listGroups);
        userGroupRepository.save(userSubscription);
    }

    public void saveSubscription(List<Integer> listGroups) {
        Set<Integer> setGroup = new HashSet<>(listGroups);
        Subscriptions subscriptions = groupsRepository.findById(1);
        if (subscriptions == null) {
            Subscriptions newSubscriptions = new Subscriptions(setGroup);
            groupsRepository.save(newSubscriptions);
        } else {
            setGroup.addAll(subscriptions.getGroups());
            subscriptions.setGroups(setGroup);
            groupsRepository.save(subscriptions);
        }
    }

    public void getSubscription (String id, VkApiClient vk, ServiceActor actor) throws ClientException, ApiException, IOException {
        List<GroupFull> group = vk.groups().getById(actor)
                .groupId(id)
                .fields(GroupField.DESCRIPTION)
                .execute();
        Gson g = new Gson();
        String data = g.toJson(group.get(0));
        elasticsearhDao.saveSubscription(id, data);
    }
}
