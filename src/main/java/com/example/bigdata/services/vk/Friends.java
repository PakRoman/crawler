package com.example.bigdata.services.vk;

import com.example.bigdata.services.elasticsearch.ElasticsearhDao;
import com.google.gson.Gson;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiAccessException;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.friends.responses.GetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class Friends {
    @Autowired
    private ElasticsearhDao elasticsearhDao;
    public void getUserFriends(int id, VkApiClient vk, ServiceActor actor) throws ClientException, ApiException, IOException {
        try {
            GetResponse response = vk.friends().get(actor)
                    .userId(id)
                    .count(9999)
                    .execute();
            List<Integer> list = response.getItems();
            Gson g = new Gson();
            String string = g.toJson(list);
            elasticsearhDao.saveFriends(String.valueOf(id), string);
        } catch (ApiAccessException e) {
            if (e.getCode() == 30) {
                System.out.println("Error");
            }
        }
    }
}
