package com.example.bigdata.services.consumer;

import com.example.bigdata.model.mongodb.*;
import com.example.bigdata.repository.ProfileTaskRepository;
import com.example.bigdata.repository.SubscriptionTaskRepository;
import com.example.bigdata.repository.VkAppRepository;
import com.example.bigdata.services.UserParser;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@EnableRabbit
@Component
public class RabbitMqConsumer {
    @Autowired
    UserParser userParser;
    @Autowired
    ProfileTaskRepository profileTaskRepository;
    @Autowired
    SubscriptionTaskRepository subscriptionTaskRepository;
    @Autowired
    VkAppRepository vkAppRepository;

    @RabbitListener(queues = "${bigdata.rabbitmq.task.profile.queue}")
    public void parsing1(ProfileTask profileTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        profileTask.setStatusTask(StatusTask.RUNNING);
        profileTask.setStartDate(startDate);
        profileTaskRepository.save(profileTask);

        for (Integer id: profileTask.getIds()) {
            userParser.profileParserWorker(id, profileTask.getVkApp().getAccess_token(), profileTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        profileTask.setStatusTask(StatusTask.CLOSED);
        profileTask.setClosedDate(closedDate);
        profileTaskRepository.save(profileTask);

        VkApp vkApp = vkAppRepository.findVkAppById(profileTask.getVkApp().getId());
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.OPEN);
        vkAppRepository.save(vkApp);
    }
    @RabbitListener(queues = "${bigdata.rabbitmq.task.profile.queue}")
    public void parsing2(ProfileTask profileTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        profileTask.setStatusTask(StatusTask.RUNNING);
        profileTask.setStartDate(startDate);
        profileTaskRepository.save(profileTask);

        for (Integer id: profileTask.getIds()) {
            userParser.profileParserWorker(id, profileTask.getVkApp().getAccess_token(), profileTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        profileTask.setStatusTask(StatusTask.CLOSED);
        profileTask.setClosedDate(closedDate);
        profileTaskRepository.save(profileTask);

        VkApp vkApp = vkAppRepository.findVkAppById(profileTask.getVkApp().getId());
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.OPEN);
        vkAppRepository.save(vkApp);
    }

    @RabbitListener(queues = "${bigdata.rabbitmq.task.subscription.queue}")
    public void parsing3(SubscriptionTask subscriptionTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.RUNNING);
        subscriptionTask.setStartDate(startDate);
        subscriptionTaskRepository.save(subscriptionTask);

        for (Integer id: subscriptionTask.getIds()) {
            userParser.subscriptionParserWorker(id, subscriptionTask.getVkApp().getAccess_token(), subscriptionTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.CLOSED);
        subscriptionTask.setClosedDate(closedDate);
        subscriptionTaskRepository.save(subscriptionTask);

        VkApp vkApp = vkAppRepository.findVkAppById(subscriptionTask.getVkApp().getId());
        Date lastUse = new Date();
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.CLOSED);
        vkApp.setLastUse(lastUse);
        vkAppRepository.save(vkApp);
    }
    @RabbitListener(queues = "${bigdata.rabbitmq.task.subscription.queue}")
    public void parsing4(SubscriptionTask subscriptionTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.RUNNING);
        subscriptionTask.setStartDate(startDate);
        subscriptionTaskRepository.save(subscriptionTask);

        for (Integer id: subscriptionTask.getIds()) {
            userParser.subscriptionParserWorker(id, subscriptionTask.getVkApp().getAccess_token(), subscriptionTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.CLOSED);
        subscriptionTask.setClosedDate(closedDate);
        subscriptionTaskRepository.save(subscriptionTask);

        VkApp vkApp = vkAppRepository.findVkAppById(subscriptionTask.getVkApp().getId());
        Date lastUse = new Date();
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.CLOSED);
        vkApp.setLastUse(lastUse);
        vkAppRepository.save(vkApp);
    }
    @RabbitListener(queues = "${bigdata.rabbitmq.task.subscription.queue}")
    public void parsing5(SubscriptionTask subscriptionTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.RUNNING);
        subscriptionTask.setStartDate(startDate);
        subscriptionTaskRepository.save(subscriptionTask);

        for (Integer id: subscriptionTask.getIds()) {
            userParser.subscriptionParserWorker(id, subscriptionTask.getVkApp().getAccess_token(), subscriptionTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.CLOSED);
        subscriptionTask.setClosedDate(closedDate);
        subscriptionTaskRepository.save(subscriptionTask);

        VkApp vkApp = vkAppRepository.findVkAppById(subscriptionTask.getVkApp().getId());
        Date lastUse = new Date();
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.CLOSED);
        vkApp.setLastUse(lastUse);
        vkAppRepository.save(vkApp);
    }
    @RabbitListener(queues = "${bigdata.rabbitmq.task.subscription.queue}")
    public void parsing6(SubscriptionTask subscriptionTask) throws ClientException, ApiException, IOException {
        Date startDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.RUNNING);
        subscriptionTask.setStartDate(startDate);
        subscriptionTaskRepository.save(subscriptionTask);

        for (Integer id: subscriptionTask.getIds()) {
            userParser.subscriptionParserWorker(id, subscriptionTask.getVkApp().getAccess_token(), subscriptionTask.getVkApp().getApp_id());
        }
        Date closedDate = new Date();
        subscriptionTask.setStatusTask(StatusTask.CLOSED);
        subscriptionTask.setClosedDate(closedDate);
        subscriptionTaskRepository.save(subscriptionTask);

        VkApp vkApp = vkAppRepository.findVkAppById(subscriptionTask.getVkApp().getId());
        Date lastUse = new Date();
        vkApp.setStatusAccountVkApp(StatusAccountVkApp.CLOSED);
        vkApp.setLastUse(lastUse);
        vkAppRepository.save(vkApp);
    }




}

