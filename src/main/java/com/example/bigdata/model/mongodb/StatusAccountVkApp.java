package com.example.bigdata.model.mongodb;

public enum StatusAccountVkApp {
    CLOSED,
    OPEN,
    RUNNING,
}
