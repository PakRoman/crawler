package com.example.bigdata.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document(collection = "groups")
public class Subscriptions {
    @Id
    private int id = 1;
    private Set<Integer> groups;

    public Subscriptions(Set<Integer> groups) {
        this.groups = groups;
    }

    public Set<Integer> getGroups() {
        return groups;
    }

    public void setGroups(Set<Integer> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Groups{" +
                "groups=" + groups +
                '}';
    }
}
