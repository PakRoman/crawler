package com.example.bigdata.model.mongodb;

public enum StatusTask {
    RUNNING,
    OPEN,
    CANCELED,
    CLOSED,
}
