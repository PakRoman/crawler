package com.example.bigdata.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "userGroup")
public class UserSubscription {
    @Id
    private Integer id;
    private List<Integer> groups;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getGroups() {
        return groups;
    }

    public void setGroups(List<Integer> groups) {
        this.groups = groups;
    }

    public UserSubscription(Integer id, List<Integer> groups) {
        this.id = id;
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "UserGroups{" +
                "id='" + id + '\'' +
                ", groups=" + groups +
                '}';
    }
}
