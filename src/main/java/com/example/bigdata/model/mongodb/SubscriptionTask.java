package com.example.bigdata.model.mongodb;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
@Document(collection = "subscriptionTask")
public class SubscriptionTask {
    @Id
    private String id;
    @Field(value = "ids")
    private List<Integer> ids;
    @Field(value = "credentionalsForVk")
    private VkApp vkApp;
    @Field(value = "status")
    private StatusTask statusTask;
    @Field(value = "createDate")
    private Date createDate;
    @Field(value = "startDate")
    private Date startDate;
    @Field(value = "closedDate")
    private Date closedDate;
    public SubscriptionTask() {
    }

    public SubscriptionTask(List<Integer> ids, StatusTask statusTask) {
        this.ids = ids;
        this.statusTask = statusTask;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }


    public StatusTask getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(StatusTask statusTask) {
        this.statusTask = statusTask;
    }

    public VkApp getVkApp() {
        return vkApp;
    }

    public void setVkApp(VkApp vkApp) {
        this.vkApp = vkApp;
    }

    @Override
    public String toString() {
        return "SubscriptionTask{" +
                "id='" + id + '\'' +
                ", ids=" + ids +
                ", vkApp=" + vkApp +
                ", statusTask=" + statusTask +
                '}';
    }
}
