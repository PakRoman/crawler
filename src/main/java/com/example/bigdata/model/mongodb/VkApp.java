package com.example.bigdata.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "vkApp")
public class VkApp {
    @Id
    private String id;
    private String access_token;
    private int app_id;
    private StatusAccountVkApp statusAccountVkApp;
    private Date lastUse;
    public VkApp() {
    }

    public VkApp(String access_token, int app_id) {
        this.access_token = access_token;
        this.app_id = app_id;
    }

    public Date getLastUse() {
        return lastUse;
    }

    public void setLastUse(Date lastUse) {
        this.lastUse = lastUse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public StatusAccountVkApp getStatusAccountVkApp() {
        return statusAccountVkApp;
    }

    public void setStatusAccountVkApp(StatusAccountVkApp statusAccountVkApp) {
        this.statusAccountVkApp = statusAccountVkApp;
    }

    @Override
    public String toString() {
        return "VkApp{" +
                "id='" + id + '\'' +
                ", access_token='" + access_token + '\'' +
                ", app_id=" + app_id +
                ", statusAccountVkApp=" + statusAccountVkApp +
                ", lastUse=" + lastUse +
                '}';
    }
}
