package com.example.bigdata;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class BigDataApplication {
    public static void main(String[] args) {

        SpringApplication.run(BigDataApplication.class, args);
    }

}
